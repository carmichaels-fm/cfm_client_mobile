
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_markdown/flutter_markdown.dart';


import '../drawer.dart' as drawer;
import '../G.dart' as G;
import '../style.dart' as S;

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {

  String content = "loading...";

  @override
  void initState() {
    super.initState();

    _load(context);
  }

  Future<String> _load(BuildContext context) async {
    var s = await DefaultAssetBundle.of(context).loadString('assets/pages/home.md');
    setState(() {
      this.content = s;
    });
  }

  void _launchURL() async {
    const url = 'https://flutter.io';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text("Carmichales FM"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.refresh), onPressed: (){
            _load(context);
          },)
        ],
      ),

      body: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Markdown(data: content))
          ],
        ),
      ),

      drawer: drawer.AppDrawer(),

      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.feedback),
        label: Text("Feedback"),
        onPressed: _launchURL
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
