
import 'package:flutter/material.dart';

import 'screens/home_screen.dart';
import 'screens/dev_screen.dart';


import 'G.dart' as G;


// main()
//Future<void> main() async {
void main() {
  //cameras = await availableCameras();
  //G.init();
  runApp( App() );
}


var ogtTheme = ThemeData(
  primarySwatch: Colors.orange,
);


class App extends StatelessWidget {
  App();



  @override
  Widget build(BuildContext context) {


    return MaterialApp(
      title: 'Carmichaels FM',
      theme: ogtTheme,

      initialRoute: G.devMode ? '/' : "/",

      routes: {
        '/':          (context) => HomeScreen(),


//        '/gps':       (context) => GpsPage(),
//        '/map':       (context) => MapPage(),
//        '/clock':     (context) => ClockScreen(),
        '/xdev':      (context) => DevScreen(),
      },

      debugShowCheckedModeBanner: false,
      //debugShowMaterialGrid: true,

    );
  }
}
