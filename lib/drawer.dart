
import 'package:flutter/material.dart';

import "style.dart" as S;


class AppDrawer extends StatelessWidget {


  @override
  Widget build(BuildContext context) {

    return Drawer(
      child: ListView(

        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(

            child: Text('Carmichaels FM', style: TextStyle(fontSize: 20),),
            decoration: BoxDecoration(
              color: S.main,

            ),
          ),


          ListTile(
            title: Text('Home'),
            leading: Icon(Icons.contacts),
            onTap: () {
              Navigator.popAndPushNamed((context), '/');
            },
          ),




          ListTile(
            title: Text('GPS'),
            leading: Icon(Icons.satellite),
            onTap: () {
              Navigator.popAndPushNamed((context), '/gps');
            },
          ),

          ListTile(
            title: Text('Map'),
            leading: Icon(Icons.map),
            onTap: () {
              Navigator.popAndPushNamed((context), '/map');
            },
          ),

          ListTile(
            title: Text('Dev Debug'),
            leading: Icon(Icons.bug_report),
            onTap: () {
              Navigator.popAndPushNamed((context), '/xdev');
            },
          ),
        ],
      ),
    );

  }
}
